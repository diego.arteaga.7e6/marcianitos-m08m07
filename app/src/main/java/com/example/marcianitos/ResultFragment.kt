package com.example.marcianitos

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.marcianitos.databinding.FragmentResultBinding

class ResultFragment : Fragment() {
    lateinit var binding: FragmentResultBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentResultBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        val totalScore = arguments?.getInt("score").toString()

        binding.score.text = totalScore



        binding.buttonMenu.setOnClickListener {

            findNavController().navigate(R.id.action_resultFragment_to_menuFragment)

        }
        binding.buttonShare.setOnClickListener {

            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, "Mira mi resultado: $totalScore")
            startActivity(Intent.createChooser(shareIntent, "Compartir con"))
        }

    }
}
