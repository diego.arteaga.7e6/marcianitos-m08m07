package com.example.marcianitos

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF

class Jugador(context: Context, screenX: Int, screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.jugador)
    val width = 150
    val height = 200
    var positionX = screenX/2
    var positionY = screenY*2/3
    var hitbox = RectF()

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width, height,false)
    }
}