package com.example.marcianitos

import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.marcianitos.databinding.FragmentMenuBinding


class MenuFragment : Fragment() {
    lateinit var binding: FragmentMenuBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMenuBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val musicMenu = MediaPlayer.create(requireContext(), R.raw.menumusic)
        musicMenu.setVolume(0.3f, 0.3f)
        musicMenu.isLooping = true
        musicMenu.start()

        binding.buttonPlay.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment_to_gameFragment)
            musicMenu.stop()

        }
    }

}